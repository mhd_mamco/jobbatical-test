import activity from './api/activity';
import users from './api/users';

export default ({ config, db, app }) => {
  
  app.use('/topActiveaUsers', activity({ config, db }));
  app.use('/users', users({ config, db }));
  
}
