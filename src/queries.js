const _limit = 5;

export const queries = {
  userQuery: `
    SELECT users.id AS id, users.name AS name, users.created_at createdAt
    FROM users
    WHERE id = $1
  `,
  companiesQuery: `
    SELECT companies.id AS id, companies.created_at AS createdAt, companies.name AS name, teams.contact_user AS isContact
    FROM companies 
    INNER JOIN teams
    ON companies.id = teams.company_id AND teams.user_id = $1
    LIMIT 
  ` + _limit,

  listingsQuery: `
    SELECT listings.id AS id, listings.created_at AS createdAt, listings.name AS name, listings.description AS description
    FROM listings 
    WHERE created_by = $1        
    LIMIT 
  ` + _limit,

  applicationsQuery: `
    SELECT applications.id AS id, applications.created_at AS createdAt, applications.cover_letter AS coverLetter,
    (SELECT listings.id  FROM listings WHERE applications.listing_id = listings.id) AS listing_id,
    (SELECT listings.name FROM listings WHERE applications.listing_id = listings.id) AS listing_name,
    (SELECT listings.description FROM listings WHERE applications.listing_id = listings.id) AS listing_description
    FROM applications
    WHERE applications.user_id = $1
    LIMIT 
  ` + _limit,

  activityQuery: `
    SELECT DISTINCT users.id AS id, users.created_at AS createdAt, users.name AS name,  
    (
      SELECT COUNT(*) FROM applications WHERE applications.user_id = users.id AND 
      applications.created_at > current_date - interval '7' day
    ) as count
    FROM users
    INNER JOIN applications
    ON users.id = applications.user_id

    WHERE applications.created_at > current_date - interval '7' day
    OFFSET $1 LIMIT $2
  `,

  listingsQuery: `
    SELECT name FROM listings
    WHERE created_at  > current_date - interval '7' day AND created_by = $1
    ORDER BY created_at DESC
    LIMIT 3
  `,

};