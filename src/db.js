import config from './config.json';
// Loading and initializing the library:
const pg = require('pg-promise');

export default callback => {
  var pgp = pg({
    // Initialization Options
  });

  // Preparing the connection details:
  const connectionString = {
    host: config.db.host,
    port: config.db.port,
    database: config.db.database,
    user: config.db.user,
    password: config.db.password,
  };
  // Creating a new database instance from the connection details:
  var db = pgp(connectionString);

  // Exporting the database object for shared use:
  callback(db);
}