import { Router } from 'express';
import { _ } from 'lodash';
import { queries } from '../queries';

export default ({ config, db }) => {
  let api = Router();

  api.get('/', (req, res) => {
    var page = req.query.page || 0,
      limit = 5,
      offset = page * limit;

    db.task(function(t) {
        // `t` and `this` here are the same;
        return t.query(queries.activityQuery, [offset, limit])
          .then(function(users) {
            let batchArr = [];
            // getting listings.
            _.each(users, user => {
              batchArr.push(t.any(queries.listingsQuery, user.id));
            })
            return t.batch(batchArr)
              .then(function(listings) {
                // merging data, `t.batch` preserve order.
                for (let key = 0; key < users.length; key++) {
                  users[key].createdAt = users[key].createdat; 
                  delete users[key].createdat;
                  users[key].listings = listings[key];
                }
                return users;
              })
          });
      })
      .then(function(events) {
        res.json(events);
      })
      .catch(function(error) {
        console.error("ERROR: ", error.message || error);
        // should return some meta data like: // not just for the failure part, as well success part too, with {error: false, data:[...]]}  
        // {
        //   error: true,
        //   data: []
        // }
        res.json([]);
      });
  });
  return api;
}
