import { Router } from 'express';
import { _ } from 'lodash';
import { queries } from '../queries';

export default ({ config, db }) => {
  let api = Router();

  api.get('/', (req, res) => {
    var userId = req.query.id;
    if(!userId) return res.json({});

    db.task(function(t) {
        // `t` and `this` here are the same;
        return t.one(queries.userQuery, userId)
          .then(function(user) {
            let batchArr = [];
            // getting companies.
            batchArr.push(t.any(queries.companiesQuery, userId));
            // getting listings.
            batchArr.push(t.any(queries.listingsQuery, userId));
            // getting applications.
            batchArr.push(t.any(queries.applicationsQuery, userId));

            return t.batch(batchArr)
              .then(function(batchResults) {
                // merging data, `t.batch` preserve order.
                user.companies = batchResults[0];
                user.createdListings = batchResults[1];
                
                user.applications = _.map(batchResults[2], item => {
                  let returnObj = _.pick(item, ['id', 'coverletter']);
                  returnObj.createdAt = item.createdat;
                  returnObj.listing = {
                    id: item.listing_id,
                    name: item.listing_name,
                    description: item.listing_description
                  }
                  return returnObj;
                });
                return user;
              })
          });
      })
      .then(function(events) {
        res.json(events);
      })
      .catch(function(error) {
        console.error("ERROR: ", error.message || error);
        // should return some meta data like {error: true, data:[]]} ; not just for the failure part, as well success part too, with {error: false, data:[...]]}  
        res.json({});
      });
  });
  return api;
}