Express & ES6 REST API
======================

# clone it
```
git clone git@bitbucket.org:mhd_mamco/jobbatical-test.git  && cd $_
```

# Install dependencies:
```
npm install
```

# Database details: 
Update `src\configs.json`  with DB `Username` and `Password`

# Start server:
```
npm start
```


# Test:
Start serveer first then:
```
mocha
```
