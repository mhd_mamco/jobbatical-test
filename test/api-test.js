process.env.NODE_ENV = 'test';

var expect = require("chai").expect;
var request = require("request");

describe('APIs', function() {
  // beforeEach: Drop all tables of testing db and  create new tables with new testable true-like data.

  var topActivity = "http://localhost:8000/topActiveUsers?page=0";
  var user = "http://localhost:8000/users?id=2";


  it("returns status 200 on GET topActiveUsers ", function() {
    request(topActivity, function(err, res, body) {
      res.should.have.status(200);
      res.should.be.json;
      res.body.should.be.a('object');
      res.body.should.have.property('id');
      res.body.should.have.property('name');
      res.body.should.have.property('count');
      res.body.should.have.property('createdAt');
      res.body.should.have.property('listings');
    });
  });

  it("returns status 200 on GET users by id ", function() {
    request(topActivity, function(err, res, body) {
      res.should.have.status(200);
      res.should.be.json;
      res.body.should.be.a('object');
      res.body.should.have.property('name');
      res.body.should.have.property('createdat');
      res.body.should.have.property('companies');
      res.body.should.have.property('createdListings');
      res.body.should.have.property('applications');
    });
  });

  it("returns status 200 on GET users without id AND empty object ", function() {
    request(topActivity, function(err, res, body) {
      res.should.have.status(200);
      expect({}).to.be.empty;
    });
  });

});